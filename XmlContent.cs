﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace oop_lab_3
{
    public class XmlContent
    {
        public string Path { get; private set; }
        public string Status { get; private set; }
        public XmlDocument Doc { get; private set; }

        public XmlContent()
        {
            Path = "";
            Status = "Error";
        }

        public void SetPath(string path)
        {
            Path = path;
        }
        public void SetStatus(string status)
        {
            Status = status;
        }
        public void SetDoc()
        {
            Doc = new XmlDocument();
            try
            {
                if (Path == "")
                {
                    Exception ex = null;
                    throw ex;
                }
                else
                {
                    Doc.Load(Path);
                    this.SetStatus("Success");
                }
            }
            catch
            {
                this.SetStatus("Error");
            }
        }

        public string GetDocString()
        {
            string ans = "";
            XmlElement xRoot = Doc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
            {
                foreach (XmlNode childnode in xnode.ChildNodes)
                    ans += childnode.Name + ": " + childnode.InnerText + "\n";
                ans += "\n";
            }
            return ans; 
        }

        public string [] GetChildnodeNames()
        {
            List<string> list = new List<string>();
            XmlElement xRoot = Doc.DocumentElement;
            foreach (XmlNode xnode in xRoot)
                foreach (XmlNode childnode in xnode.ChildNodes)
                    if(!list.Contains(childnode.Name))
                        list.Add(childnode.Name);
            string [] ans = list.ToArray();
            return ans;
        }
        public string[][] GetChildnodeValues(int size)
        {
            List<List<string>> list = new List<List<string>>();
            for (int i = 0; i < size; i++)
                list.Add(new List<string>());

            XmlElement xRoot = Doc.DocumentElement;
            for (int k = 0; k < xRoot.ChildNodes.Count; k++)
            {
                for (int i = 0; i < xRoot.ChildNodes[k].ChildNodes.Count; i++)
                {
                    XmlNode childnode = xRoot.ChildNodes[k].ChildNodes[i];
                    if (!list[i].Contains(""))
                        list[i].Add("");
                    for (int j = 0; j < childnode.ChildNodes.Count; j++)
                        if (!list[i].Contains(childnode.ChildNodes[j].InnerText))
                            list[i].Add(childnode.ChildNodes[j].InnerText);
                }
            }
            List<string[]> help = new List<string[]>();
            for (int i = 0; i < size; i++)
            {
                list[i].Sort();
                help.Add(list[i].ToArray());
            }
            string[][] ans = help.ToArray();
            return ans;
        }
    };
}
