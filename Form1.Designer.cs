﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace oop_lab_3
{
    partial class XMLParser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private List<ComboBox> CreateComboBoxes(int x, int y, int w, int h, string[][] childs, string[] names)
        {
            List<ComboBox> comboBoxes = new List<ComboBox>();
            for (int i = 0; i < names.Length; i++)
            {
                var comboBox = new System.Windows.Forms.ComboBox();
                comboBox.Location = new System.Drawing.Point(x, y + i * 20);
                comboBox.Name = "comboBox" + (i + 1);
                comboBox.Size = new System.Drawing.Size(w, h);
                comboBox.Items.AddRange(childs[i]);
                comboBoxes.Add(comboBox);
                this.Controls.Add(comboBox);
            }
            return comboBoxes;
        }

        private List<Label> CreateTextBoxes(int x, int y, int w, int h, string[] names)
        {
            List<Label> labels = new List<Label>();
            for (int i = 0; i < names.Length; i++)
            {
                var label = new System.Windows.Forms.Label();
                label.Location = new System.Drawing.Point(x, y + i * 20);
                label.Name = "label" + (i + 1);
                label.Size = new System.Drawing.Size(w, h);
                label.Text = names[i];
                labels.Add(label);
                this.Controls.Add(label);
            }
            return labels;
        }

        private void DeleteComboBoxes(List<ComboBox> comboBoxes)
        {
            foreach (ComboBox comboBox in comboBoxes)
                this.Controls.Remove(comboBox);
        }

        private void DeleteLabels(List<Label> labels)
        {
            foreach (Label label in labels)
                this.Controls.Remove(label);
        }

        private void InitializeComponent()
        {
            this.saxButton = new System.Windows.Forms.RadioButton();
            this.domButton = new System.Windows.Forms.RadioButton();
            this.linqButton = new System.Windows.Forms.RadioButton();
            this.AnalysisType = new System.Windows.Forms.GroupBox();
            this.parse = new System.Windows.Forms.Button();
            this.path = new System.Windows.Forms.TextBox();
            this.pathtothefile = new System.Windows.Forms.Label();
            this.LoadFile = new System.Windows.Forms.Button();
            this.xmlContent = new System.Windows.Forms.RichTextBox();
            this.AnalysisType.SuspendLayout();
            this.SuspendLayout();
            // 
            // saxButton
            // 
            this.saxButton.AutoSize = true;
            this.saxButton.Location = new System.Drawing.Point(6, 21);
            this.saxButton.Name = "saxButton";
            this.saxButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.saxButton.Size = new System.Drawing.Size(56, 21);
            this.saxButton.TabIndex = 0;
            this.saxButton.TabStop = true;
            this.saxButton.Text = "SAX";
            this.saxButton.UseVisualStyleBackColor = true;
            this.saxButton.CheckedChanged += new System.EventHandler(this.saxButton_CheckedChanged);
            // 
            // domButton
            // 
            this.domButton.AutoSize = true;
            this.domButton.Location = new System.Drawing.Point(68, 21);
            this.domButton.Name = "domButton";
            this.domButton.Size = new System.Drawing.Size(61, 21);
            this.domButton.TabIndex = 1;
            this.domButton.TabStop = true;
            this.domButton.Text = "DOM";
            this.domButton.UseVisualStyleBackColor = true;
            this.domButton.CheckedChanged += new System.EventHandler(this.domButton_CheckedChanged);
            // 
            // linqButton
            // 
            this.linqButton.AutoSize = true;
            this.linqButton.Location = new System.Drawing.Point(135, 21);
            this.linqButton.Name = "linqButton";
            this.linqButton.Size = new System.Drawing.Size(61, 21);
            this.linqButton.TabIndex = 2;
            this.linqButton.TabStop = true;
            this.linqButton.Text = "LINQ";
            this.linqButton.UseVisualStyleBackColor = true;
            this.linqButton.CheckedChanged += new System.EventHandler(this.linqButton_CheckedChanged);
            // 
            // AnalysisType
            // 
            this.AnalysisType.Controls.Add(this.saxButton);
            this.AnalysisType.Controls.Add(this.linqButton);
            this.AnalysisType.Controls.Add(this.domButton);
            this.AnalysisType.Location = new System.Drawing.Point(12, 48);
            this.AnalysisType.Name = "AnalysisType";
            this.AnalysisType.Size = new System.Drawing.Size(200, 53);
            this.AnalysisType.TabIndex = 3;
            this.AnalysisType.TabStop = false;
            this.AnalysisType.Text = "Analysis type";
            // 
            // parse
            // 
            this.parse.Location = new System.Drawing.Point(228, 415);
            this.parse.Name = "parse";
            this.parse.Size = new System.Drawing.Size(75, 30);
            this.parse.TabIndex = 4;
            this.parse.Text = "Parse";
            this.parse.UseVisualStyleBackColor = true;
            this.parse.Click += new System.EventHandler(this.parse_Click);
            // 
            // path
            // 
            this.path.Location = new System.Drawing.Point(112, 12);
            this.path.Name = "path";
            this.path.Size = new System.Drawing.Size(127, 22);
            this.path.TabIndex = 5;
            this.path.TextChanged += new System.EventHandler(this.path_TextChanged);
            // 
            // pathtothefile
            // 
            this.pathtothefile.AutoSize = true;
            this.pathtothefile.Location = new System.Drawing.Point(9, 15);
            this.pathtothefile.Name = "pathtothefile";
            this.pathtothefile.Size = new System.Drawing.Size(105, 17);
            this.pathtothefile.TabIndex = 6;
            this.pathtothefile.Text = "Path to the xml:";
            // 
            // LoadFile
            // 
            this.LoadFile.Location = new System.Drawing.Point(245, 12);
            this.LoadFile.Name = "LoadFile";
            this.LoadFile.Size = new System.Drawing.Size(58, 23);
            this.LoadFile.TabIndex = 7;
            this.LoadFile.Text = "Load";
            this.LoadFile.UseVisualStyleBackColor = true;
            this.LoadFile.Click += new System.EventHandler(this.LoadFile_Click);
            // 
            // xmlContent
            // 
            this.xmlContent.Location = new System.Drawing.Point(309, 12);
            this.xmlContent.Name = "xmlContent";
            this.xmlContent.Size = new System.Drawing.Size(479, 426);
            this.xmlContent.TabIndex = 8;
            this.xmlContent.Text = "";
            // 
            // XMLParser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.xmlContent);
            this.Controls.Add(this.LoadFile);
            this.Controls.Add(this.path);
            this.Controls.Add(this.pathtothefile);
            this.Controls.Add(this.parse);
            this.Controls.Add(this.AnalysisType);
            this.Name = "XMLParser";
            this.Text = "XMLParser";
            this.Load += new System.EventHandler(this.XMLParser_Load);
            this.AnalysisType.ResumeLayout(false);
            this.AnalysisType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton saxButton;
        private System.Windows.Forms.RadioButton domButton;
        private System.Windows.Forms.RadioButton linqButton;
        private System.Windows.Forms.GroupBox AnalysisType;
        private System.Windows.Forms.Button parse;
        private System.Windows.Forms.TextBox path;
        private System.Windows.Forms.Label pathtothefile;
        private System.Windows.Forms.Button LoadFile;
        private System.Windows.Forms.RichTextBox xmlContent;
    }
}

