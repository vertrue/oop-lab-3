﻿namespace oop_lab_3
{
    public class AnalysisType
    {
        public string Type { get; private set; }

        public AnalysisType()
        {
            Type = "unknown";
        }

        public AnalysisType(string type)
        {
            Type = type;
        }
    };
}
