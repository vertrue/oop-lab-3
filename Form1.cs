﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace oop_lab_3
{
    public partial class XMLParser : Form
    {
        Parser MainParser = new Parser();
        AnalysisType Type = new AnalysisType();
        XmlContent Xml = new XmlContent();
        HelpfulForms Forms = new HelpfulForms();
        Exception ex;

        public Exception Ex { get => ex; set => ex = value; }

        private void XMLParser_Load(object sender, EventArgs e)
        {
        }

        public XMLParser()
        {
            InitializeComponent();
        }

        private void domButton_CheckedChanged(object sender, EventArgs e)
        {
            MainParser.SetParserType(new DomParser());
        }

        private void saxButton_CheckedChanged(object sender, EventArgs e)
        {
            MainParser.SetParserType(new SaxParser());
        }

            private void linqButton_CheckedChanged(object sender, EventArgs e)
        {
            MainParser.SetParserType(new LinqParser());
        }

        private void path_TextChanged(object sender, EventArgs e)
        {
            Xml.SetPath(path.Text); 
        }

        private void LoadFile_Click(object sender, EventArgs e)
        {
            Xml.SetDoc();
            //MessageBox.Show(text: Xml.Status);
            if (Xml.Status == "Error")
                MessageBox.Show(text: Xml.Status);
            else
            {
                DeleteComboBoxes(Forms.comboBoxes);
                DeleteLabels(Forms.labels);

                xmlContent.Text = Xml.GetDocString();
                int x = 12, y = 90, w = 60, h = 20;
                var names = Xml.GetChildnodeNames();
                var values = Xml.GetChildnodeValues(names.Length);

                Forms.setLabels(CreateTextBoxes(x, y + 4, w, h, names));
                Forms.setComboBoxes(CreateComboBoxes(x + 60, y, h + 120, h, values, names));

                MainParser.SetDoc(Xml.Path);
            }

        }

        private void parse_Click(object sender, EventArgs e)
        {
            if (Xml.Status != "Success")
            {
                MessageBox.Show(text: "Please, load a xml-file");
                return;
            }
            List<string> list = new List<string>();
            //try
            {
                list = MainParser.Parse(Forms.labels, Forms.comboBoxes);
                string ans = "";
                for (int i = 0; i < list.Count; i++)
                    ans += list[i] + "\n";
                xmlContent.Text = ans;
            }
            //catch
            //{
            //    MessageBox.Show(text: "Error");
            //}
        }
    }
}
