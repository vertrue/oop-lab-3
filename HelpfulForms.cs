﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace oop_lab_3
{
    public class HelpfulForms
    {
        public List<ComboBox> comboBoxes { get; private set; }
        public List<Label> labels { get; private set; }

        public HelpfulForms()
        {
            comboBoxes = new List<ComboBox>();
            labels = new List<Label>();
        }

        public void setComboBoxes(List<ComboBox> boxes)
        {
            comboBoxes = boxes;
        }

        public void setLabels(List<Label> _labels)
        {
            labels = _labels;
        }
    };
}
