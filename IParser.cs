﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace oop_lab_3
{
    interface IParser
    {
        List<string> Parse(string path, List<Label> labels, List<ComboBox> comboBoxes);
    }

    class DomParser : IParser
    {
        public List<string> Parse(string path, List<Label> labels, List<ComboBox> comboBoxes)
        {
            List<string> ans = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(path);

            XmlElement xRoot = doc.DocumentElement;
            bool flag;
            foreach (XmlNode xnode in xRoot)
            {
                flag = true;
                string locans = "";
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    string name = childnode.Name, value = childnode.InnerText;
                    for (int i = 0; i < labels.Count; i++)
                        if (labels[i].Text == name)
                            if (comboBoxes[i].SelectedItem != null)
                                if (comboBoxes[i].SelectedItem.ToString() != value && comboBoxes[i].SelectedItem.ToString() != "")
                                    flag = false;
                    locans += name + ": " + value + "\n";
                }
                if(flag)
                    ans.Add(locans);
            }

            return ans;
        }
    }

    class SaxParser : IParser
    {
        public List<string> Parse(string path, List<Label> labels, List<ComboBox> comboBoxes)
        {
            List<string> ans = new List<string>();
            using (XmlReader xml = XmlReader.Create(path))
            {
                int depth = 0;
                string locans = "";
                string name = "", value = "";
                bool flag = true;
                while (xml.Read())
                {
                    if(xml.IsStartElement())
                    {
                        depth++;
                        if (depth > 2)
                        {
                            name = xml.Name;
                            if (xml.IsEmptyElement)
                                value = "";
                        }
                    }
                    else if (xml.NodeType == XmlNodeType.Text)
                    {
                        if (depth > 2)
                            value = xml.Value;
                    }
                    else if (xml.NodeType == XmlNodeType.EndElement)
                    {
                        depth--;
                        if (depth == 1)
                        {
                            if (flag)
                                ans.Add(locans);
                            else
                                flag = true;
                            locans = "";
                        }
                        else if (depth == 2)
                        {
                            for (int i = 0; i < labels.Count; i++)
                                if (labels[i].Text == name)
                                    if (comboBoxes[i].SelectedItem != null)
                                        if (comboBoxes[i].SelectedItem.ToString() != value && comboBoxes[i].SelectedItem.ToString() != "")
                                        {
                                            flag = false;
                                            locans = "";
                                        }
                            locans += name + ": " + value + "\n";
                        }
                    }
                }
            }
            return ans;
        }
    }

    class LinqParser : IParser
    {
        public List<string> Parse(string path, List<Label> labels, List<ComboBox> comboBoxes)
        {
            XDocument doc = XDocument.Load(path);
            List<string> ans = new List<string>();

            var zashto = (from el in doc.Elements()
                          select new {Name = (el.FirstNode as XElement).Name.ToString()});

            string name = zashto.ToArray()[0].Name;

            var items = (from el in doc.Descendants(name)
                         select el).ToList();

            for (int i = 0; i < labels.Count; i++)
                if(comboBoxes[i].SelectedItem != null)
                    if(comboBoxes[i].SelectedItem.ToString() != "")
                        items = (from el in items
                                 where el.Element(labels[i].Text).Value == comboBoxes[i].SelectedItem.ToString()
                                select el).ToList();

            foreach (var el in items)
            {
                string locans = "";
                foreach (var x in el.Elements())
                    locans += x.Name + ": " + x.Value + "\n";
                ans.Add(locans);
            }

            return ans;
        }
    }

    class Parser
    {
        public IParser ParserType { get; private set; }
        public string Path { get; private set; }

        public Parser()
        {
        }

        public void SetParserType(IParser parser)
        {
            ParserType = parser;
        }

        public void SetDoc(string path)
        {
            Path = path;
        }

        public List<string> Parse(List<Label> labels, List<ComboBox> comboBoxes)
        {
            return ParserType.Parse(Path, labels, comboBoxes);
        }
    }
}
